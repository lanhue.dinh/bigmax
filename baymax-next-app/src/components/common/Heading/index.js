'use client';
import React from 'react';
import { Typography } from 'antd';

const { Title, Paragraph } = Typography;

function Heading({ title, description }) {
  return (
    <div>
      <Title level={2}>{title}</Title>
      <Paragraph>{description}</Paragraph>
    </div>
  );
}

export default Heading;