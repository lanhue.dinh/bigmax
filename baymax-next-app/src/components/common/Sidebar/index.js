"use client";
import React from 'react';
import { Layout, Menu } from 'antd';
import Link from 'next/link';
import {
  DiscordOutlined,
  DashboardOutlined,
  UserOutlined,
  ShoppingOutlined,
  CommentOutlined,
  PictureOutlined,
  ProductOutlined,
  FolderViewOutlined,
  HighlightOutlined
} from '@ant-design/icons';
import "./index.css";

const { Sider } = Layout;

const menuItems = [
  { key: '1', icon: <DashboardOutlined />, label: 'Dashboard', href: '/' },
  { key: '2', icon: <UserOutlined />, label: 'Users', href: '/users' },
  { key: '3', icon: <ProductOutlined />, label: 'Products', href: '/products' },
  { key: '4', icon: <FolderViewOutlined />, label: 'Albums', href: '/albums' },
  { key: '5', icon: <PictureOutlined />, label: 'Photos', href: '/photos' },
  { key: '6', icon: <HighlightOutlined />, label: 'Posts', href: '/posts' },
  { key: '7', icon: <CommentOutlined />, label: 'Comments', href: '/comments' }
];

const Sidebar = () => {
  return (
    <Sider width={200} className="site-layout-background" style={{ position: 'fixed', height: '100vh', zIndex: 10 }}>
      <div className="logo" style={{ color: "white", padding: '16px', textAlign: 'center', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        <DiscordOutlined style={{ fontSize: '32px', marginRight: '8px' }} />
        <span style={{ fontSize: '24px', fontWeight: 'bold' }}>BAYMAX</span>
      </div>
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={['4']}
        style={{ height: '100%', borderRight: 0 }}
      >
        {menuItems.map(item => (
          <Menu.Item key={item.key} icon={item.icon}>
            <Link href={item.href}>{item.label}</Link>
          </Menu.Item>
        ))}
      </Menu>
    </Sider>
  );
};

export default Sidebar;