'use client';

import Image from 'next/image';
import { Row, Col, Table, Typography } from 'antd';

const { Title } = Typography;

function PhotoList({ data }) {
  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Album Id',
      dataIndex: 'albumId',
      key: 'albumId',
    },
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Url',
      dataIndex: 'url',
      key: 'url',
      render: (_, record) => (
        <div style={{ width: '80px' }}>
          <Image
            width={80}
            height={80}
            src={record.url}
            alt={record.title}
            style={{ width: '100%' }}
          />
        </div>
      ),
    },
    {
      title: 'Thumbnail Url',
      dataIndex: 'thumbnailUrl',
      key: 'thumbnailUrl',
      render: (_, record) => (
        <div style={{ width: '80px' }}>
          <Image
            width={80}
            height={80}
            src={record.thumbnailUrl}
            alt={record.title}
            style={{ width: '100%' }}
          />
        </div>
      ),
    },
  ];

  return (
    <div className="h-full w-full">
      <Row gutter={24}>
        <Col span={24}>
          <Title level={3}>Manage Photos</Title>
          <div className="flex-1 pt-6">
            <Table
              rowKey="id"
              dataSource={data}
              columns={columns}
              pagination={{
                pageSizeOptions: [50, 100, 200, 500],
                defaultPageSize: 20,
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default PhotoList;