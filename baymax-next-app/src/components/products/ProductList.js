'use client';

import { useEffect, useState, useCallback } from 'react';
import { Row, Col, Table, Typography, Spin, List, Avatar } from 'antd';

const { Title } = Typography;

function ProductList({ data }) {
  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
    },
    {
      title: 'Category',
      dataIndex: 'category',
      key: 'category',
    },
    {
      title: 'Brand',
      dataIndex: 'brand',
      key: 'brand',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
    },
  ];

  return (
    <div className="h-full w-full">
      <Row gutter={24}>
        <Col span={24}>
          <Title level={3}>Manage Products</Title>
          <div className="flex-1 pt-6">
            <Table
              rowKey="id"
              dataSource={data}
              columns={columns}
              pagination={{
                pageSizeOptions: [50, 100, 200, 500],
                defaultPageSize: 100,
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default ProductList;