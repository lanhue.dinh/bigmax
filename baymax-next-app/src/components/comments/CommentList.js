'use client';

import { useEffect, useState, useCallback } from 'react';
import { Row, Col, Table, Typography, Spin, List, Avatar } from 'antd';

const { Title } = Typography;

function CommentList({ data }) {
  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Post ID',
      dataIndex: 'postId',
      key: 'postId',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Comments',
      dataIndex: 'body',
      key: 'body',
    },
  ];

  return (
    <div className="h-full w-full">
      <Row gutter={24}>
        <Col span={24}>
          <Title level={3}>Manage Comments</Title>
          <div className="flex-1 pt-6">
            <Table
              rowKey="id"
              dataSource={data}
              columns={columns}
              pagination={{
                pageSizeOptions: [50, 100, 200, 500],
                defaultPageSize: 100,
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default CommentList;