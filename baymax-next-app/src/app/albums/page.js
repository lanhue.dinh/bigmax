import { Metadata } from 'next';
import AlbumList from '@/components/albums/AlbumList';
import { Spin } from 'antd';

export const metadata = {
  title: "Baymax | Albums",
  description: "Generated by create next app",
};

export default async function page() {
  const response = await fetch("https://jsonplaceholder.typicode.com/albums");
  const albums = await response.json();
  if (!albums) {
    return <Spin tip="Loading albums..." />;
  }
  return (
    <div>
      <AlbumList data={albums} />
    </div>
  );
}