import React, { useState } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Layout } from 'antd';
import Sidebar from '../src/components/sidebar';
import DashboardPage from '../src/pages/dashboard';
import UserPage from '../src/pages/users';
import ProductPage from '../src/pages/products';
import AlbumPage from '../src/pages/albums';
import PhotoPage from '../src/pages/photos';
import PostPage from '../src/pages/posts';
import CommentPage from '../src/pages/comments';


const { Header, Content, Footer } = Layout;

const App = () => {
  const [selectedPage, setSelectedPage] = useState('5');

  const renderPage = () => {
    switch (selectedPage) {
      case '1':
        return <DashboardPage />;
      case '2':
        return <UserPage />;
      case '3':
        return <ProductPage />;
      case '4':
        return <AlbumPage />;
      case '5':
        return <PhotoPage />;
      case '6':
        return <PostPage />;
      case '7':
        return <CommentPage />;
      default:
        return <DashboardPage />;
    }
  };

  return (
    <Router>
      <Layout style={{ height: '100vh' }}>
        <Sidebar onSelect={setSelectedPage} style={{ position: 'fixed', height: '100vh' }} />
        <Layout style={{ padding: '0 24px 24px', marginLeft: "200px", background: "white" }}>
          <Content
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
              height: "100%"
            }}
          >
            {renderPage()}
          </Content>
        </Layout>
      </Layout>
      <Footer style={{ textAlign: 'center', width: "100%" }}>Baymax ©2024 Created by Dinh Thi Lan Hue</Footer>
    </Router>
  );
};

export default App;