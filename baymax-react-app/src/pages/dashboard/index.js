import React from 'react';
import { Row, Col, Card, Table, Calendar, Typography } from 'antd';
import { Line, Bar, Pie } from '@ant-design/charts';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';

const { Title } = Typography;

const DashboardPage = () => {
  const lineConfig = {
    data: [
      { month: 'Jan', value: 3 },
      { month: 'Feb', value: 4 },
      { month: 'Mar', value: 3.5 },
      { month: 'Apr', value: 5 },
      { month: 'May', value: 4.9 },
      { month: 'Jun', value: 6 },
      { month: 'Jul', value: 7 },
      { month: 'Aug', value: 9 },
      { month: 'Sep', value: 13 },
      { month: 'Oct', value: 12 },
      { month: 'Nov', value: 10 },
      { month: 'Dec', value: 14 },
    ],
    xField: 'month',
    yField: 'value',
    point: {
      size: 5,
      shape: 'diamond',
    },
  };

  const barConfig = {
    data: [
      { type: 'Category 1', value: 38 },
      { type: 'Category 2', value: 52 },
      { type: 'Category 3', value: 61 },
      { type: 'Category 4', value: 145 },
      { type: 'Category 5', value: 48 },
      { type: 'Category 6', value: 38 },
      { type: 'Category 7', value: 38 },
      { type: 'Category 8', value: 38 },
    ],
    xField: 'value',
    yField: 'type',
    seriesField: 'type',
    legend: {
      position: 'top-left',
    },
  };

  const pieConfig = {
    appendPadding: 10,
    data: [
      { type: 'Category 1', value: 27 },
      { type: 'Category 2', value: 25 },
      { type: 'Category 3', value: 18 },
      { type: 'Category 4', value: 15 },
      { type: 'Category 5', value: 10 },
      { type: 'Other', value: 5 },
    ],
    angleField: 'value',
    colorField: 'type',
    radius: 1,
    innerRadius: 0.64,
    label: {
      content: ({ percent }) => `${(percent * 100).toFixed(0)}%`,
      style: {
        fontSize: 14,
        textAlign: 'center',
      },
    },
    interactions: [],
    statistic: {
      title: false,
      content: {
        style: {
          whiteSpace: 'pre-wrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
        content: 'Total\n1234',
      },
    },
  };

  const userColumns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Date Joined',
      dataIndex: 'dateJoined',
      key: 'dateJoined',
    },
  ];

  const productColumns = [
    {
      title: 'Product Name',
      dataIndex: 'productName',
      key: 'productName',
    },
    {
      title: 'Category',
      dataIndex: 'category',
      key: 'category',
    },
    {
      title: 'Date Added',
      dataIndex: 'dateAdded',
      key: 'dateAdded',
    },
  ];

  const userData = [
    {
      key: '1',
      name: 'John Doe',
      email: 'john@example.com',
      dateJoined: '2024-08-01',
    },
    {
      key: '2',
      name: 'Jane Smith',
      email: 'jane@example.com',
      dateJoined: '2024-08-02',
    },
    {
      key: '3',
      name: 'Sam Brown',
      email: 'sam@example.com',
      dateJoined: '2024-08-03',
    },
  ];

  const productData = [
    {
      key: '1',
      productName: 'Product A',
      category: 'Category 1',
      dateAdded: '2024-08-01',
    },
    {
      key: '2',
      productName: 'Product B',
      category: 'Category 2',
      dateAdded: '2024-08-02',
    },
    {
      key: '3',
      productName: 'Product C',
      category: 'Category 3',
      dateAdded: '2024-08-03',
    },
  ];

  return (
    <div className="dashboard">
      <Row gutter={[16, 16]}>
        <Col span={24}>
          <Card title="Charts" bordered={false}>
            <Row gutter={[16, 16]}>
              <Col span={8}>
                <Card title="Line Chart" bordered={false}>
                  <Line {...lineConfig} />
                </Card>
              </Col>
              <Col span={8}>
                <Card title="Bar Chart" bordered={false}>
                  <Bar {...barConfig} />
                </Card>
              </Col>
              <Col span={8}>
                <Card title="Pie Chart" bordered={false}>
                  <Pie {...pieConfig} />
                </Card>
              </Col>
            </Row>
          </Card>
        </Col>

        <Col span={24}>
          <Card title="Analytics Overview" bordered={false}>
            <Row gutter={[16, 16]}>
              <Col span={6}>
                <Card title="Total Users" bordered={false}>
                  <Title level={3}>1,234</Title>
                </Card>
              </Col>
              <Col span={6}>
                <Card title="Active Users" bordered={false}>
                  <Title level={3}>567</Title>
                </Card>
              </Col>
              <Col span={6}>
                <Card title="New Signups" bordered={false}>
                  <Title level={3}>89</Title>
                </Card>
              </Col>
              <Col span={6}>
                <Card title="Revenue" bordered={false}>
                  <Title level={3}>$12,345</Title>
                </Card>
              </Col>
            </Row>
          </Card>
        </Col>

        <Col span={24}>
          <Card title="Calendar" bordered={false}>
            <Calendar fullscreen={false} />
          </Card>
        </Col>

        <Col span={24}>
          <Card title="New Users and Products" bordered={false}>
            <Row gutter={[16, 16]}>
              <Col span={12}>
                <Card title="New Users" bordered={false}>
                  <Table columns={userColumns} dataSource={userData} pagination={false} />
                </Card>
              </Col>
              <Col span={12}>
                <Card title="New Products" bordered={false}>
                  <Table columns={productColumns} dataSource={productData} pagination={false} />
                </Card>
              </Col>
            </Row>
          </Card>
        </Col>

        <Col span={24}>
          <Card title="Visitors Map" bordered={false}>
            <MapContainer center={[51.505, -0.09]} zoom={13} style={{ height: '400px' }}>
              <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              />
              <Marker position={[51.505, -0.09]}>
                <Popup>
                  A pretty CSS3 popup. <br /> Easily customizable.
                </Popup>
              </Marker>
            </MapContainer>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default DashboardPage;