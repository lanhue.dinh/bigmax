import { useEffect, useState, useCallback } from 'react';
import { Row, Col, Table, Typography, Spin } from 'antd';
import SearchComponent from "../../components/search";

const { Title } = Typography;

export default function UserPage() {
  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [loading, setLoading] = useState(true);

  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
    },
    {
      title: 'Category',
      dataIndex: 'category',
      key: 'category',
    },
    {
      title: 'Brand',
      dataIndex: 'brand',
      key: 'brand',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
    },
  ];

  const fetchProducts = useCallback(async () => {
    try {
      const response = await fetch("https://dummyjson.com/products");
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      const data = await response.json();
      setData(data.products);
      setFilteredData(data.products);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    fetchProducts();
  }, [fetchProducts]);

  const handleSearch = (value, key) => {
    if (!value) {
      setFilteredData(data);
      return;
    }
    const filtered = data.filter(item =>
      item[key].toLowerCase().includes(value.toLowerCase())
    );
    setFilteredData(filtered);
  };

  if (loading) {
    return <Spin tip="Loading..." />;
  }

  if (!data.length) {
    return <p>No data found</p>;
  }

  return (
    <div className="h-full w-full">
      <Row gutter={24}>
        <Col span={24}>
          <Title level={3}>Manage Products</Title>
          <div className="flex-1 pt-6">
            <SearchComponent onSearch={handleSearch} searchKey="title" />
            <Table
              rowKey="id"
              dataSource={filteredData}
              columns={columns}
              pagination={{
                pageSizeOptions: [50, 100, 200, 500],
                defaultPageSize: 100,
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}