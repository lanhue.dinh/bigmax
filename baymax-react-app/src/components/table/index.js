import React, { useState } from 'react';
import { Theme, Table, Flex } from '@radix-ui/themes';
// import { ChevronLeftIcon, ChevronRightIcon } from 'lucide-react';

const DataTable = ({
  columns,
  data,
  searchKey,
  totalRecords,
  pageCount,
  pageSizeOptions = [10, 20, 30, 40, 50],
}) => {
  const [searchTerm, setSearchTerm] = useState('');
  const [pageIndex, setPageIndex] = useState(0);
  const [pageSize, setPageSize] = useState(pageSizeOptions[0]);
  let filteredData = [];
  
  if(searchKey === ""){
    filteredData = data;
  }else {
      filteredData = data?.filter((item) =>
      item[searchKey]?.toLowerCase().includes(searchTerm?.toLowerCase())
    );
  }

  const startIndex = pageIndex * pageSize;
  const endIndex = Math.min(startIndex + pageSize, filteredData?.length);
  const pageData = filteredData?.slice(startIndex, endIndex);

  const goToPage = (index) => {
    if (index >= 0 && index < pageCount) {
      setPageIndex(index);
    }
  };

  return (
    <div>
      <br />
      <Theme>
        <Flex direction="column">
          <div className="rounded-sm border">
            <Table.Root className="relative">
              <Table.Header>
                <Table.Row>
                  {columns?.map((column, index) => (
                    <Table.ColumnHeaderCell key={index} className="table-header-cell">
                      {typeof column?.header === 'function'
                        ? column?.header({ table: filteredData })
                        : column?.header}
                    </Table.ColumnHeaderCell>
                  ))}
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {pageData?.map((item, rowIndex) => (
                  <Table.Row key={rowIndex}>
                    {columns?.map((column, colIndex) => (
                      <Table.Cell key={colIndex} className="table-cell">
                        {typeof column?.cell === 'function'
                          ? column?.cell({ row: item })
                          : item[column?.accessorKey]}
                      </Table.Cell>
                    ))}
                  </Table.Row>
                ))}
              </Table.Body>
            </Table.Root>
          </div>
        </Flex>
        <div className="flex flex-col items-center justify-end gap-2 space-x-2 py-4 sm:flex-row">
          <div className="flex w-full items-center justify-between">
            <div className="flex-1 text-sm text-muted-foreground">
              {pageData?.length} of {filteredData?.length} row(s) displayed.
            </div>
            <div className="flex flex-col items-center gap-4 sm:flex-row sm:gap-6 lg:gap-8">
              <div className="flex items-center space-x-2">
                <p className="whitespace-nowrap text-sm font-medium">Rows per page</p>
                <select
                  value={pageSize}
                  onChange={(e) => {
                    setPageSize(Number(e.target.value));
                    setPageIndex(0); // Reset to first page on page size change
                  }}
                  className="h-8 w-[70px]"
                >
                  {pageSizeOptions?.map((size) => (
                    <option key={size} value={size}>
                      {size}
                    </option>
                  ))}
                </select>
              </div>
            </div>
          </div>
          <div className="flex w-full items-center justify-between gap-2 sm:justify-end">
            <div className="flex w-[100px] items-center justify-center text-sm font-medium">
              Page {pageIndex + 1} of {pageCount}
            </div>
            <div className="flex items-center space-x-2">
              <button
                aria-label="Go to first page"
                className="hidden h-8 w-8 p-0 lg:flex"
                onClick={() => goToPage(0)}
                disabled={pageIndex === 0}
              >
              </button>
              <button
                aria-label="Go to previous page"
                className="h-8 w-8 p-0"
                onClick={() => goToPage(pageIndex - 1)}
                disabled={pageIndex === 0}
              >
              </button>
              <button
                aria-label="Go to next page"
                className="h-8 w-8 p-0"
                onClick={() => goToPage(pageIndex + 1)}
                disabled={pageIndex >= pageCount - 1}
              >
              </button>
              <button
                aria-label="Go to last page"
                className="hidden h-8 w-8 p-0 lg:flex"
                onClick={() => goToPage(pageCount - 1)}
                disabled={pageIndex >= pageCount - 1}
              >
              </button>
            </div>
          </div>
        </div>
      </Theme>
    </div>
  );
};

export default DataTable;