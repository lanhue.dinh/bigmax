import React from 'react';
import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';
import {
  DiscordOutlined,
  DashboardOutlined,
  UserOutlined,
  ShoppingOutlined,
  CommentOutlined,
  PictureOutlined,
  ProductOutlined,
  FolderViewOutlined,
  HighlightOutlined
} from '@ant-design/icons';

const { Sider } = Layout;

const Sidebar = ({ onSelect }) => {
  return (
    <Sider width={200} className="site-layout-background" style={{ position: 'fixed', height: '100vh', "z-index": "10" }}>
      <div className="logo" style={{ color: "white", padding: '16px', textAlign: 'center', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        <DiscordOutlined style={{ fontSize: '32px', marginRight: '8px' }} />
        <span style={{ fontSize: '24px', fontWeight: 'bold' }}>BAYMAX</span>
      </div>
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={['4']}
        style={{ height: '100%', borderRight: 0 }}
        onClick={(e) => onSelect(e.key)}
      >
        <Menu.Item key="1" icon={<DashboardOutlined />}>
          <Link to="/dashboard">Dashboard</Link>
        </Menu.Item>
        <Menu.Item key="2" icon={<UserOutlined />}>
          <Link to="/users">Users</Link>
        </Menu.Item>
        <Menu.Item key="3" icon={<ProductOutlined />}>
          <Link to="/products">Products</Link>
        </Menu.Item>
        <Menu.Item key="4" icon={<FolderViewOutlined />}>
          <Link to="/albums">Albums</Link>
        </Menu.Item>
        <Menu.Item key="5" icon={<PictureOutlined />}>
          <Link to="/photos">Photos</Link>
        </Menu.Item>
        <Menu.Item key="6" icon={<HighlightOutlined />}>
          <Link to="/posts">Posts</Link>
        </Menu.Item>
        <Menu.Item key="7" icon={<CommentOutlined />}>
          <Link to="/comments">Comments</Link>
        </Menu.Item>
      </Menu>
    </Sider>
  );
};

export default Sidebar;