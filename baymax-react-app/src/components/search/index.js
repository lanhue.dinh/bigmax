import React, { useState } from 'react';
import { Input } from 'antd';

const { Search } = Input;

const SearchComponent = ({ onSearch, searchKey }) => {
  const handleInputChange = (e) => {
    const value = e.target.value;
    onSearch(value, searchKey);
  };

  return (
    <Search
      placeholder={`Search by ${searchKey}...`}
      onChange={handleInputChange}
      style={{ width: "100%", marginBottom: 16 }}
    />
  );
};

export default SearchComponent;